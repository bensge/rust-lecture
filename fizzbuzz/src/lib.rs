//! This module contains everything you need to build your own fizzbuzz program.

/// generates a fizzbuzz string for one iteration with number n.
pub fn fizzbuzz(n: u32) -> String {
    if n % 3 == 0 && n % 5 == 0 {
        "FizzBuzz".to_string()
    } else if n % 3 == 0 {
        "Fizz".to_string()
    } else if n % 5 == 0 {
        "Buzz".to_string()
    } else {
        n.to_string()
    }
}
