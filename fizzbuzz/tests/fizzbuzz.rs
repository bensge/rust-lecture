use fizzbuzz::fizzbuzz;

macro_rules! acceptance_test {
    ($suite:ident, $($name:ident: $input:expr, $output:expr,)*) => {
        mod $suite {
            use super::*;
            $(
                #[test]
                fn $name() -> () {
                    let out = fizzbuzz($input);
                    assert_eq!($output, out);
                }
            )*
        }
    };
}

acceptance_test!(simple,
    one: 1, "1",
    two: 2, "2",
    three: 3, "Fizz",
    four: 4, "4",
    five: 5, "Buzz",
    six: 6, "Fizz",
    nine: 9, "Fizz",
    fifteen: 15, "FizzBuzz",
);
